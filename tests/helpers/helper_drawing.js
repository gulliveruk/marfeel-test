define(['d3'], function (d3) {

  function createSvg(elementID,width,height){
    width = width || 200;
    height = height || 300;
    return d3.select(elementID)
      .append('svg')
      .attr('width', width)
      .attr('height', height);
  }
  function removeSvg(svg){
    var child = svg[0][0];
    var parent = child.parentNode;
    parent.removeChild(child);
  }
  function removeAllSvg(){
    d3.selectAll('svg').remove();
  }
  function returnAllSVGElements(svg){
    var elements = svg[0][0].childNodes;
    return elements;
  }
  function returnCoordsFromElem(elem){
    var attribs,spl;
    switch(elem.tagName){
      case "path":
        attribs = elem.attributes[0].value;
        spl = attribs.split(" ");
               return  {
                 x1 : parseInt(spl[1],10),
                 x2 : parseInt(spl[4],10),
                 y1 : parseInt(spl[2],10),
                 y2 : parseInt(spl[5],10)
               };
      case "line":
                return  {
        x1 : parseInt(elem.attributes[0].value,10),
        x2 : parseInt(elem.attributes[2].value,10),
        y1 : parseInt(elem.attributes[1].value,10),
        y2 : parseInt(elem.attributes[3].value,10)
      };
      case "text":
        return  {
          x : parseInt(elem.attributes[0].value,10),
          y : parseInt(elem.attributes[1].value,10)
        };
    }

  }


  return {
    createSvg:createSvg,
    removeSvg:removeSvg,
    removeAllSvg:removeAllSvg,
    returnAllSVGElements:returnAllSVGElements,
    returnCoordsFromElem:returnCoordsFromElem
  };
});



