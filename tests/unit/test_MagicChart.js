define([
  'intern!tdd',
  'intern/chai!assert',
  'app/components/MagicChart',
  'app/data/visits',
  'tests/helpers/helper_MagicChart'
], function (tdd, assert, MagicChart,visitsData,help_chart) {
  tdd.suite('MagicChart methods ', function () {
    var testDiv,testData;
    tdd.beforeEach(function () {
      testDiv = document.createElement('div');
      testDiv.id = 'testDiv';
      document.body.appendChild(testDiv);
      testData = help_chart.cloneObject(visitsData);
    });
    tdd.afterEach(function () {
      document.body.removeChild(testDiv);
    });

    tdd.suite('MagicChart', function () {
      tdd.test('can add a chart to div', function () {
        new MagicChart(testData,'#testDiv');
        var elems = testDiv.childNodes;
        assert.equal(elems[0].tagName, 'svg','svg missing');
      });
      tdd.test('can add a chart to div', function () {
        new MagicChart(testData,'#testDiv');
        var elems = testDiv.childNodes;
        assert.equal(elems[0].tagName, 'svg','svg missing');
      });
      tdd.test('has all required groups', function () {
        new MagicChart(testData,'#testDiv');
        var elems = testDiv.childNodes[0].childNodes;
        assert.equal(elems[0].attributes['class'].value, 'trendLine','trendLine missing');
        assert.equal(elems[1].attributes['class'].value, 'doughnut','doughnut missing');
        assert.equal(elems[2].attributes['class'].value, 'decoration','decoration missing');
        assert.equal(elems[3].attributes['class'].value, 'reticle','reticle missing');
        assert.equal(elems[4].attributes['class'].value, 'highlight','highlight missing');
      });

    });



  });
});



