define([
  'intern!tdd',
  'intern/chai!assert',
  'app/helpers/drawing',
  'tests/helpers/helper_drawing'
], function (tdd, assert, drawing,help_draw) {
  tdd.suite('drawing module methods ', function () {
    var svgCanvas;
    tdd.beforeEach(function () {
      svgCanvas = help_draw.createSvg('body');
    });
    tdd.afterEach(function () {
      help_draw.removeAllSvg();
    });
    tdd.suite('setup tests', function () {
      tdd.test('should be no elements on canvas to begin with', function () {
        var elems = help_draw.returnAllSVGElements(svgCanvas);
        assert.equal(elems.length, 0,'elements were found - canvas should be empty');
      });
    });
    tdd.suite('drawLine', function () {
      tdd.test('should draw a single line', function () {
        drawing.drawLine(svgCanvas, 10,  15,120, 250);
        var elems = help_draw.returnAllSVGElements(svgCanvas);
        assert.equal(elems.length, 1,'canvas should have one element');
      });
      tdd.test('should have the right coordinates', function () {
        drawing.drawLine(svgCanvas, 10,  15,120, 250);
        var elems = help_draw.returnAllSVGElements(svgCanvas);
        var coords = help_draw.returnCoordsFromElem(elems[0]);
        assert.deepEqual(coords, {x1:10,x2:15,y1:120,y2:250},'coords incorrect for element');
      });


    });
   tdd.suite('addText', function () {
      tdd.test('should add text element', function () {
        drawing.addText(svgCanvas,20,30,'our text');
        var elems = help_draw.returnAllSVGElements(svgCanvas);
        assert.equal(elems.length, 1,'canvas should have one element');
        assert.equal(elems[0].tagName, 'text','the element isnt text');
      });
      tdd.test('should have the right coordinates', function () {
        drawing.addText(svgCanvas,23,32,'our text2');
        var elems = help_draw.returnAllSVGElements(svgCanvas);
        var coords = help_draw.returnCoordsFromElem(elems[0]);
        assert.deepEqual(coords, {x:23,y:32},'coords incorrect for element');
      });
     tdd.test('should have the right text content', function () {
        drawing.addText(svgCanvas,23,32,'our text3');
        var elems = help_draw.returnAllSVGElements(svgCanvas);
        assert.deepEqual(elems[0].textContent, 'our text3','text incorrect for element');
      });


    });



  });
});



