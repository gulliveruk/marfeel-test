define([
  'intern!tdd',
  'intern/chai!assert',
  'tests/helpers/helper_drawing'
], function (tdd, assert, help_draw) {
  tdd.suite('drawing helper module methods ', function () {


    tdd.suite('setup tests', function () {
      tdd.test('should be no svg elements on page to start', function () {
        var elems = document.body.getElementsByTagName("svg");
        assert.equal(elems.length, 0,'svg elements were found');
      });
    });

    tdd.suite('createSvg', function () {
      tdd.test('should add an svg element to body', function () {
        var ret = help_draw.createSvg('body');
        var elems = document.body.getElementsByTagName("svg");
        assert.ok(ret,'svg element not returned');
        assert.equal(elems.length, 1,'1 svg element isnt found');
        assert.equal(elems[0].tagName, 'svg','the element isnt svg');
        help_draw.removeAllSvg();
      });
      tdd.test('should add multiple svg element to body', function () {
        var ret1 = help_draw.createSvg('body');
        var ret2 = help_draw.createSvg('body');
        var elems = document.body.getElementsByTagName("svg");
        assert.ok(ret1,'1st svg element not returned');
        assert.ok(ret2,'2nd svg element not returned');
        assert.equal(elems.length, 2,'2 svg elements not found');
        help_draw.removeAllSvg();
      });

      tdd.test('should add an svg element to div element', function () {
        var div = document.createElement('div');
        var ret = help_draw.createSvg(div);
        var elems = div.childNodes;
        assert.ok(ret,'svg element not returned');
        assert.equal(elems.length, 1,'1 element isnt found');
        assert.equal(elems[0].tagName, 'svg','the element isnt svg');
      });
      tdd.test('should add an svg element to div element of correct size', function () {
        var div = document.createElement('div');
        help_draw.createSvg(div,500,220);
        var elems = div.childNodes;
        assert.equal(elems[0].attributes.width.value, 500,'width is wrong');
        assert.equal(elems[0].attributes.height.value, 220,'height is wrong');
      });
    });
    tdd.suite('removeAllSvg', function () {
      tdd.test('should remove multiple svg elements on body', function () {
        help_draw.createSvg('body');
        help_draw.createSvg('body');
        help_draw.removeAllSvg();
        var elems = document.body.getElementsByTagName("svg");
        assert.equal(elems.length, 0,'svg elements found');
      });

    });
    tdd.suite('removeSvg', function () {
      tdd.test('should remove single svg elements on body using dom element', function () {
        var ret1 = help_draw.createSvg('body');
        help_draw.createSvg('body');
        help_draw.removeSvg(ret1);
        var elems = document.body.getElementsByTagName("svg");
        assert.equal(elems.length, 1,'1 svg element not found');
      });

    });
    tdd.suite('returnAllSVGElements', function () {
      tdd.test('should return nothing on empty canvas', function () {
        var ret1 = help_draw.createSvg('body');
        var elems = help_draw.returnAllSVGElements(ret1);
        assert.equal(elems.length, 0,'drawing elemets found');
        help_draw.removeAllSvg();
      });
      tdd.test('should return a single element on svg', function () {
        var ret1 = help_draw.createSvg('body');
        var newElement = document.createElementNS("http://www.w3.org/2000/svg", 'path');
        newElement.setAttribute("d","M 0 0 L 10 10");
        newElement.style.stroke = "#000";
        newElement.style.strokeWidth = "5px";
        ret1[0][0].appendChild(newElement);
        var elems = help_draw.returnAllSVGElements(ret1);
        assert.equal(elems.length, 1,'1 drawing elemet not found');
        assert.equal(elems[0].tagName, 'path','the element isnt a path');
        help_draw.removeAllSvg();
      });
      tdd.test('should return multiple elements on svg', function () {
        var ret1 = help_draw.createSvg('body');
        var newElement = document.createElementNS("http://www.w3.org/2000/svg", 'path');
        newElement.setAttribute("d","M 0 0 L 10 10");
        newElement.style.stroke = "#000";
        newElement.style.strokeWidth = "5px";
        ret1[0][0].appendChild(newElement);
        newElement = document.createElementNS("http://www.w3.org/2000/svg", 'path');
        newElement.setAttribute("d","M 20 0 L 15 10");
        ret1[0][0].appendChild(newElement);
        var elems = help_draw.returnAllSVGElements(ret1);
        assert.equal(elems.length, 2,'2 drawing elements not found');
        assert.equal(elems[0].tagName, 'path','the element isnt a path');
        help_draw.removeAllSvg();
      });

    });
    tdd.suite('returnCoordsFromElem', function () {
      tdd.test('should return correct coords from d element', function () {
        var ret1 = help_draw.createSvg('body');
        var newElement = document.createElementNS("http://www.w3.org/2000/svg", 'path');
        newElement.setAttribute("d","M 0 100 L 10 15");
        ret1[0][0].appendChild(newElement);
        var elems = help_draw.returnAllSVGElements(ret1);
        var coords = help_draw.returnCoordsFromElem(elems[0]);
        assert.deepEqual(coords, {x1:0,x2:10,y1:100,y2:15},'coords incorrect for element');
        help_draw.removeAllSvg();
      });
      tdd.test('should return correct coords from line element', function () {
        var ret1 = help_draw.createSvg('body');
        var aLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        aLine.setAttribute('x1', 22);
        aLine.setAttribute('y1', 34);
        aLine.setAttribute('x2', 102);
        aLine.setAttribute('y2', 276);
        ret1[0][0].appendChild(aLine);
        var elems = help_draw.returnAllSVGElements(ret1);
        var coords = help_draw.returnCoordsFromElem(elems[0]);
        assert.deepEqual(coords, {x1:22,x2:102,y1:34,y2:276},'coords incorrect for element');
        help_draw.removeAllSvg();
      });
    });



  });
});



