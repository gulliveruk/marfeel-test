define([
  'intern!tdd',
  'intern/chai!assert',
  'app/helpers/common'
], function (tdd, assert, common) {
  tdd.suite('common methods ', function () {

    tdd.before(function () {

    });

    tdd.suite('numberFormat', function () {
      tdd.test('doesnt affect numbers less than 4 digits', function () {
        assert.equal(common.numberFormat(300), '300');
      });
      tdd.test('does affect numbers with 4 digits', function () {
        assert.equal(common.numberFormat(1000), '1.000');
      });
      tdd.test('does affect numbers with 7 digits', function () {
        assert.equal(common.numberFormat(1000000), '1.000.000');
      });
      tdd.test('can add a prefix', function () {
        assert.equal(common.numberFormat(1000000, '$'), '$1.000.000');
      });
      tdd.test('can add a postfix', function () {
        assert.equal(common.numberFormat(1000000,'', '$'), '1.000.000$');
      });

    });
    tdd.suite('calcTotal', function () {
      tdd.test('sums object with 2 fields', function () {
        var obj = [{value:100,label:'man'},{value:120,label:'woman'}];
        assert.equal(common.calcTotal(obj,'value'), 220);
      });
      tdd.test('sums object with 3 fields', function () {
        var obj = [{value:100,label:'man'},{value:120,label:'woman'},{value:43,label:'cat'}];
        assert.equal(common.calcTotal(obj,'value'), 263);
      });
      tdd.test('sums object with 5 fields', function () {
        var obj = [{value:100,label:'man'},{value:120,label:'woman'},{value:43,label:'cat'},{value:100,label:'face'},{value:91,label:'head'},{value:21,label:'leg'}];
        assert.equal(common.calcTotal(obj,'value'), 475);
      });
      tdd.test('sums object with field missing', function () {
        var obj = [{value:100,label:'man'},{value:120,label:'woman'},{length:43,label:'cat'}];
        assert.equal(common.calcTotal(obj,'value'), 220);
      });
      tdd.test('sums object with negative value', function () {
        var obj = [{length:-100,label:'man'},{length:120,label:'woman'},{length:43,label:'cat'}];
        assert.equal(common.calcTotal(obj,'length'), 63);
      });
      tdd.test('sums object with invalid value', function () {
        var obj = [{length:100,label:'man'},{length:120,label:'woman'},{length:'hat',label:'cat'}];
        assert.equal(common.calcTotal(obj,'length'), 220);
      });
      tdd.test('sums object but ignores numbers in strings', function () {
        var obj = [{length:100,label:'man'},{length:120,label:'woman'},{length:'100',label:'cat'}];
        assert.equal(common.calcTotal(obj,'length'), 220);
      });

    });
    tdd.suite('calculateStats', function () {
      var obj,res;
      tdd.test('calculates total', function () {
        obj = [{value:100,label:'man'},{value:120,label:'woman'}];
        res = common.calculateStats(obj,'value');
        assert.equal(res, 220);
      });
      tdd.test('calculates percentages', function () {
        obj = [{value:100,label:'man'},{value:200,label:'woman'}];
        res = common.calculateStats(obj,'value');
        assert.deepEqual(obj, [{value:100,label:'man',percent: 33.33333333333333},{value:200,label:'woman',percent: 66.66666666666666}]);
      });
      tdd.test('calculates percentages when a field is missing', function () {
        obj = [{value:100,label:'man'},{moomin:200,label:'woman'}];
        res = common.calculateStats(obj,'value');
        assert.deepEqual(obj, [{value:100,label:'man',percent: 100},{moomin:200,label:'woman'}]);
      });

    });


  });
});



