define([], function () {
  "use strict";
  return {
    title:"IMPRESSIONS",
    headline: [{label: 'Smartphone', count: 30000000},
      {label: 'Tablet', count: 20000000}],
    series: [{"date": "1-May-12", "close": "1053.13"}, {"date": "30-Apr-12", "close": "840.98"}, {
      "date": "27-Apr-12",
      "close": "850"
    }, {"date": "26-Apr-12", "close": "800.70"}, {"date": "25-Apr-12", "close": "790.00"}, {
      "date": "24-Apr-12",
      "close": "788"
    }, {"date": "23-Apr-12", "close": "786.70"}, {"date": "20-Apr-12", "close": "777.98"}, {
      "date": "19-Apr-12",
      "close": "750"
    }, {"date": "18-Apr-12", "close": "440.34"}, {"date": "17-Apr-12", "close": "743.70"}, {
      "date": "16-Apr-12",
      "close": "720.13"
    }, {"date": "13-Apr-12", "close": "495.23"}, {"date": "12-Apr-12", "close": "682.77"}, {
      "date": "11-Apr-12",
      "close": "680.20"
    }, {"date": "10-Apr-12", "close": "478.44"}, {"date": "9-Apr-12", "close": "636.23"}, {
      "date": "5-Apr-12",
      "close": "633.68"
    }, {"date": "4-Apr-12", "close": "564.31"}, {"date": "3-Apr-12", "close": "529.32"}, {
      "date": "2-Apr-12",
      "close": "518.63"
    }, {"date": "30-Mar-12", "close": "499.55"}, {"date": "29-Mar-12", "close": "459.86"}, {
      "date": "28-Mar-12",
      "close": "417.62"
    }, {"date": "27-Mar-12", "close": "404.48"}, {"date": "26-Mar-12", "close": "406.98"}]
  };
});
