define([], function () {
  "use strict";
  return {
    title:"VISITS",
    headline: [{label: 'Smartphone', count: 120000000},
      {label: 'Tablet', count: 480000000}],
    series: [{"date": "1-May-12", "close": "653.13"}, {"date": "30-Apr-12", "close": "840.98"}, {
      "date": "27-Apr-12",
      "close": "850"
    }, {"date": "26-Apr-12", "close": "600.70"}, {"date": "25-Apr-12", "close": "790.00"}, {
      "date": "24-Apr-12",
      "close": "788"
    }, {"date": "23-Apr-12", "close": "686.70"}, {"date": "20-Apr-12", "close": "777.98"}, {
      "date": "19-Apr-12",
      "close": "750"
    }, {"date": "18-Apr-12", "close": "640.34"}, {"date": "17-Apr-12", "close": "743.70"}, {
      "date": "16-Apr-12",
      "close": "720.13"
    }, {"date": "13-Apr-12", "close": "695.23"}, {"date": "12-Apr-12", "close": "682.77"}, {
      "date": "11-Apr-12",
      "close": "680.20"
    }, {"date": "10-Apr-12", "close": "478.44"}, {"date": "9-Apr-12", "close": "636.23"}, {
      "date": "5-Apr-12",
      "close": "633.68"
    }, {"date": "4-Apr-12", "close": "664.31"}, {"date": "3-Apr-12", "close": "529.32"}, {
      "date": "2-Apr-12",
      "close": "518.63"
    }, {"date": "30-Mar-12", "close": "799.55"}, {"date": "29-Mar-12", "close": "459.86"}, {
      "date": "28-Mar-12",
      "close": "417.62"
    }, {"date": "27-Mar-12", "close": "904.48"}, {"date": "26-Mar-12", "close": "406.98"}]
  };
});
