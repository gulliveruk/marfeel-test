define(["d3", "app/helpers/drawing", "app/helpers/common"], function (d3, drawing, common) {
  "use strict";
  function MagicChart(data, elementId, options) {
    if (!(this instanceof MagicChart)) {
      throw new TypeError("MagicChart constructor cannot be called as a function.");
    }
    this.data = data.headline;
    this.elementId = elementId;
    this.o = options || {};
    this.o.width = this.o.width || 179;
    this.o.height = this.o.height || this.o.width;
    this.o.canvasWidth = this.o.canvasWidth || this.o.width * 1.6;
    this.o.canvasHeight = this.o.canvasHeight || this.o.height * 1.5;

    this.o.doughnutWidth = this.o.doughnutWidth || this.o.width / 24;
    this.o.radius = this.o.radius || Math.min(this.o.width, this.o.height) / 2;
    this.o.margin = this.o.margin || this.o.radius / 24;

    var xCenter = this.o.canvasWidth / 2
      , yCenter = (this.o.radius + this.o.doughnutWidth)
      , margin = this.o.doughnutWidth
      , topPos = yCenter - this.o.radius + margin
      , botPos = yCenter + this.o.radius - margin
      , leftPos = xCenter - this.o.radius + margin
      , rightPos = xCenter + this.o.radius - margin
      , total = common.calculateStats(this.data, "count");

    var svgCanvas = d3.select(this.elementId)
      .append('svg')
      .attr('width', this.o.canvasWidth)
      .attr('height', this.o.canvasHeight);
    this.svg = svgCanvas[0];
    drawTrendLine(svgCanvas, this, data.series);
    drawDoughnut(svgCanvas, this.data, this.o.radius, this.o.doughnutWidth);
    addRingAndLabel(svgCanvas, this.o.radius, this.o.doughnutWidth, xCenter, yCenter);
    drawReticle(svgCanvas, this.o.doughnutWidth / 2);
    drawHighlight(svgCanvas, this.data, this.o.canvasWidth, this.o.canvasHeight, this.o.radius);


    function drawReticle(svg, length) {
      var retGroup = svg.append('g').attr('class', 'reticle');
      drawing.drawLine(retGroup, xCenter, xCenter, topPos, topPos + length);
      drawing.drawLine(retGroup, xCenter, xCenter, botPos, botPos - length);
      drawing.drawLine(retGroup, leftPos, leftPos + length, yCenter, yCenter);
      drawing.drawLine(retGroup, rightPos, rightPos - length, yCenter, yCenter);
    }


    function drawTrendLine(svg, context, dataToPlot) {
      var margin = {top: 0 + context.o.radius, right: context.o.margin * 2, bottom: 30, left: context.o.margin * 2},
        aWidth = (rightPos - leftPos) - margin.left - margin.right,
        aHeight = (context.o.radius * 2) - margin.top - margin.bottom;

      var x = d3.time.scale().range([0, aWidth]);
      var y = d3.scale.linear().range([aHeight, 0]);
      var parseDate = d3.time.format("%d-%b-%y").parse;

      var valueline = d3.svg.line()
        .x(function (d) {
          return x(d.date);
        })
        .y(function (d) {
          return y(d.close);
        });

      var area = d3.svg.area()
        .x(function (d) {
          return x(d.date);
        })
        .y0(function (d) {
          return returnRadius(x(d.date), y(d.close), context.o.radius - context.o.margin - context.o.doughnutWidth, -0);
        })
        .y1(function (d) {
          return y(d.close);
        });

      var trendGroup = svg.append('g')
        .attr('class', 'trendLine')
        .attr("transform",
        "translate(" + (leftPos + margin.left ) + "," + (topPos + margin.top ) + ")");


      dataToPlot.forEach(function (d) {
        d.date = parseDate(d.date);
        d.close = +d.close;
      });

      x.domain(d3.extent(dataToPlot, function (d) {
        return d.date;
      }));
      y.domain([0, d3.max(dataToPlot, function (d) {
        return d.close;
      })]);

      trendGroup.append("path")
        .datum(dataToPlot)
        .attr("class", "area")
        .attr("d", area);

      trendGroup.append("path")
        .attr("d", valueline(dataToPlot))
        .attr("class", "arealine")
        .attr("fill", "none");
    }


    function drawHighlight(svg, dataToAdd, width, height, radius) {
      var offset = radius * 1.1;
      var halfWidth = width / 2;
      var hlSVG = svg.append('g')
        .attr('class', 'highlight')
        .attr('transform', 'translate(' + xCenter + ',' + (yCenter + offset) + ')');
      drawing.drawLine(hlSVG, -halfWidth, halfWidth, 45, 45);
      drawing.addText(hlSVG, -halfWidth, 5, dataToAdd[1].label, {textanchor: 'left', className: 'label-1'});
      drawing.addHTMLElement(hlSVG, -halfWidth, 12, width, 20, '<div class="breakOutWrap"><div class="cat1">' + dataToAdd[1].percent + '% <span class="value">' + common.numberFormat(dataToAdd[1].count, (data.title === 'REVENUE') ? '$' : '') + '</span></div><div class="cat2">' + dataToAdd[0].percent + '% <span class="value">' + common.numberFormat(dataToAdd[0].count, (data.title === 'REVENUE') ? '$' : '') + '</span></div></div>');
      drawing.addText(hlSVG, halfWidth, 5, dataToAdd[0].label, {textanchor: 'end', className: 'label-0'});

    }

    function addRingAndLabel(svg, radius, doughnutWidth, xCenter, yCenter) {
      var strokeWidth = doughnutWidth * 1.1;
      var ringAndLabel = svgCanvas.append('g')
        .attr('class', 'decoration');
      ringAndLabel.append('circle')
        .attr('cx', xCenter)
        .attr('cy', yCenter)
        .attr('r', radius - doughnutWidth - (strokeWidth / 2))
        //.attr('r', 52)
        .style('fill', 'none')
        .style('stroke', 'white')
        .style('stroke-opacity', '1')
        .style('stroke-width', strokeWidth + 'px');
      drawing.addText(ringAndLabel, xCenter, yCenter + 10, common.numberFormat(total, '', (data.title === 'REVENUE') ? '€' : ''), {textanchor: 'middle', className: 'label-value'});
      drawing.addText(ringAndLabel, xCenter, yCenter - 20, data["title"], {textanchor: 'middle', className: 'label-title'});

    }

    function drawDoughnut(svg, dataToPlot, radius, doughnutWidth) {
      var thisGroup = svg.append('g')
        .attr('class', 'doughnut')
        .attr('transform', 'translate(' + xCenter + ',' + yCenter + ')');
      var arc = d3.svg.arc()
        .innerRadius(radius - doughnutWidth)
        .outerRadius(radius);
      var pie = d3.layout.pie()
        .value(function (d) {
          return d.count;
        })
        .sort(null);

      thisGroup.append('g').selectAll('path')
        .data(pie(dataToPlot))
        .enter()
        .append('path')
        .attr('d', arc)
        .attr('class', function (d, i) {
          return 'series-' + i;
        });


    }

    function returnRadius(xAxes, yVal, radius, sagitta) {
      var offset = (xAxes / radius) - 1;
      var val = (Math.cos(offset) * radius) + sagitta;
      if (val < yVal) {
        val = yVal;
      }
      //console.log(xAxes +':x offset ' +offset  + ' radius ' + radius+ ' val ' + val + ' sagita '+sagitta + ' yval ' + yVal);
      return val;
    }
  }

  return MagicChart;
});
