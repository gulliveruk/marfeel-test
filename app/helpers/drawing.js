define(["d3"], function () {
  "use strict";
  function drawLine(svg,startX,endX,startY,endY,opts){
    opts = opts || {};
    opts.stroke  = opts.stroke || 'black';
    opts.strokewidth = opts.strokewidth ||'1px';
    opts.strokeopacity = opts.strokeopacity || '1';
  svg.append('line')
    .attr('x1', startX)
    .attr('y1', startY)
    .attr('x2', endX)
    .attr('y2', endY)
    .style('stroke', opts.stroke)
    .style('stroke-opacity', opts.strokeopacity)
    .style('stroke-width', opts.strokewidth);
  }

  function addText(svg,xPos,yPos,text,opts){
    opts = opts || {};
    opts.textanchor  = opts.textanchor || 'left';
    opts.className  = opts.className || 'label';
    svg.append("text")
      .attr("dx", xPos)
      .attr("dy", yPos)
      .attr('class',opts.className)
      .style("text-anchor", opts.textanchor)
      .text(text);
  }
  function addHTMLElement(svg,xPos,yPos,width,height,html,opts){
    opts = opts || {};
    opts.textanchor  = opts.textanchor || 'left';
    opts.className  = opts.className || 'label';
    svg.append("foreignObject")
      .attr({
            height: height,
            width: width,
            transform: 'translate(' + xPos +',' + yPos + ')'
          })
      .attr('class',opts.className)
      .append("xhtml:div")
      .html(html);
  }
  //function addGroup(svg,xTrans,yTrans,opts){
  //
  //}
  return {
    drawLine :drawLine,
    addText: addText,
    addHTMLElement:addHTMLElement
  };
});
