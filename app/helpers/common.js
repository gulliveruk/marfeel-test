define([], function () {
"use strict";
  function calculateStats(dataObj,field){
        var tot = calcTotal(dataObj,field);
        calcPerc(dataObj,field,tot);
    return tot;
  }

  function calcTotal(dataObj,field){
    var tot = 0;
    dataObj.forEach(function(a){
      if(a[field] && typeof a[field]==='number'){
        tot = tot + a[field];
      }
    });
    return tot;
  }

  function calcPerc(dataObj,field,tot){
    dataObj.forEach(function(each,idx){
      if(each[field] && typeof each[field]==='number') {
        dataObj[idx].percent = (each[field] / tot) * 100;
      }
    });
  }

  function numberFormat(number,prefix,postfix){
    prefix = prefix || '';
    postfix = postfix || '';
    return prefix + number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + postfix;
  }


  return {
    calcTotal : calcTotal,
    calculateStats: calculateStats,
    numberFormat:numberFormat
  };

});
