define(['swiper'], function (Swiper) {
  "use strict";
  var ourSlider,slidesPerView = 2;

  function getWindowWidth(){
    return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  }

  function initSlider(elementID){
    ourSlider = new Swiper(elementID, {
      pagination: '.swiper-pagination',
      slidesPerView: slidesPerView,
      centeredSlides: (slidesPerView < 3),
      paginationClickable: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      spaceBetween: 20,
      freeMode: true,
      grabCursor: true
    });
    updateSlider();
  }
  function calcSlidesPerPage(windowWidth,slideWidth){
    var width = windowWidth * 0.95;
    return Math.floor(windowWidth/slideWidth);
  }
  function updateSlider(){
    if(ourSlider){
      slidesPerView = calcSlidesPerPage(getWindowWidth(),400);
      ourSlider.params.slidesPerView = slidesPerView;
      ourSlider.params.spaceBetween = slidesPerView * 5;
      ourSlider.params.centeredSlides = (slidesPerView < 3);

      ourSlider.update(true);
    }

  }
  function autoUpdateSlider(){
    addResizeEventHandler(updateSlider);
  }


  function addResizeEventHandler(callback){
    (function() {
      var timer_id;
      window.addEventListener("resize", function() {
        clearTimeout(timer_id);
        timer_id = setTimeout(function() {
          callback();
        }, 300);
      });
    })();
  }

  return {
    initSlider:initSlider,
    getWindowWidth:getWindowWidth,
    calcSlidesPerPage:calcSlidesPerPage,
    updateSlider:updateSlider,
    autoUpdateSlider:autoUpdateSlider
  };

});
