define(["components/magicChart","data/revenue","data/impressions","data/visits","helpers/slider"], function (MagicChart,revenue,impressions,visits,slider) {
  "use strict";
  var chart1 = new MagicChart(revenue,'#chart1');
  console.log('chart 1 is at ' + chart1.svg[0].offsetLeft + ' : ' + chart1.svg[0].offsetTop);
  var chart2 = new MagicChart(impressions,'#chart2');
  console.log('chart 2 is at ' + chart2.svg[0].offsetLeft + ' : ' + chart2.svg[0].offsetTop);
  var chart3 = new MagicChart(visits,'#chart3');
  console.log('chart 3 is at ' + chart3.svg[0].offsetLeft + ' : ' + chart3.svg[0].offsetTop);

  slider.initSlider('.swiper-container');
  slider.autoUpdateSlider();

});
