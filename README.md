Marfeel test assignment

Things to note:

I hadn't used D3 before, and after figuring out how to create the various charts, i had to write tests against what i'd already done, rather than actually test driving the development process of the charts.  Also, to properly test drive D3, I'd want to write test helpers to really understand that the elements which should be together are together for example.
The MagicChart module isn't as clean as I'd like, it's a bit of a controller&view - perhaps more Knockout.js style - It needs a bit more refactoring, but i figured at least you get to see a bit of the working process.

I spent a lot of time trying to get the bottom of the area chart to perfectly follow the line of the circle below it - but in the end I just covered it up with a white circle.

I used Intern as a testing framework as i like how it's already all based around AMD modules, and I really like how it integrates with sauce labs for cross browser testing, plus it's use of promises for async things is cool.
Running all the tests is done by going to
node_modules/intern/client.html?config=tests/intern.js

I put some time into making the component able to scale fluidly - by adjusting the value of MagicChart.o.width will scale the entire chart and it's elements together - well apart from the font sizes as they're set in css and I didnt want to start overriding them as that could cause confusion.
